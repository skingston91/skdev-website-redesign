var buttonRef;

$( document ).ready(function() {

	$('#content-div').hide();
      $("#logo").fadeIn(3000, function()
	  {
		$(".ca-menu").fadeIn();
      });
	  
	//CreatePortfolioImageArray();
	CreateProjects();
});

//List of all our Projects
var portfolioProjectArray = [];

//Create all the projects 
function CreateProjects(){
	CreateProject("About-Me","Me.JPG","https://twitter.com/StevenKingston1","About Me","Welcome to my Website, I Program,travel Game and Procrastinate? This is a place to showcase anything and everything.","image");
	CreateProject("Protest","Protest.png","Protest-Jam.html","Unity Project Protest","Created in Unity4 for Edge's get into Games competition. This features a fully functional dynamic story driven game with multiple endings. \
				This was created with a group of friends in our spare time in 2 months.","image");
	CreateProject("IndieBooks","indieBooks.jpg","http://skdev.co.uk/indieBooks/index.php","E-Commerce Site: Indie-Books","Created for an University E-Commerce assessment, the site features ssl encryption, \
	downloadable text files, secure-logins, integration with Google checkout and secure Audit logs and Trail.","image");
	CreateProject("BettingGame","Race.png","betting-game/Game.html","CBR - Racing Betting Game","Created in a weekend to learn canvas, this contains a name generator, and a fully functional betting system","image");
	CreateProject("ProjectIcarus","ProjectIcarus.png","https://www.youtube.com/watch?v=BMAfZBcVpCg","Current Project: Unity 3D RPG ","The long term project, working as a part of a small team building a prototype of a fully 3D cinematic role playing game including an active battle system and exploration. \
	In it's current form, it contains a working battle system, dynamic exploration,functional menus, music and sound.","video");
	CreateProject("Careers-App","careers-app.png","http://skdev.co.uk/careersapp/loginPage.php","Responsive Careers App","Created for a school in Canterbury as part of final year two man group project.\
	This followed a full project life cycle from interviews with teachers and students to integration into the schools eco-system. This was a Nine Month Project.","image","248","510");
}
//Create the project and add it to the array
function CreateProject(name,image,link,title,content,type,imageWidth,imageHeight){
	var folder = 'img/Portfolio/';
	var absoluteImage = folder.concat(image);
	var project = new Project(name,absoluteImage,link,title,content,type,imageWidth,imageHeight);
	portfolioProjectArray.push(project);
}

//When menu selection buttons is pressed which searches through the array
// for the project using its name  and calls the required methods
// Logs an error if not found 
function MenuOptionButton(keyName)
{
	var found = false;
	for (i = 0; i < portfolioProjectArray.length; i++) { 
		var project = portfolioProjectArray[i];
		if(project.name == keyName){
			found = true;
			ChangeContentPanel(project);
			DefineButtonLink(project.link);
			break;
		}
	}
	if(found = false){
		console.log("Project Not Found");
	}
}

//this will modify the text in the div to the project we selected
function ChangeContentPanel(project)
{
	$('#content-div').show();
	document.getElementById("content-title").innerHTML = project.title;
	document.getElementById("content-text").innerHTML = project.content;
	document.getElementById("content-image").src = project.image;
	document.getElementById("content-image").height = project.imageHeight;
	document.getElementById("content-image").width = project.imageWidth;
	console.log(project.link);
	document.getElementById("content-image").href = project.link;
}


//Store the link as a global variable ready to be used by the button
function DefineButtonLink(link){
	buttonRef = link; // set the new page we will go to
}

//Page the button will redirect to 
function NewPage(){
	window.location = buttonRef;
}
