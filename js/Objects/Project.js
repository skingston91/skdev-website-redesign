// Created as the Object to store data of each Portfolio 
// Steven Kingston 26/06/2015

function Project (name, image, link,title,content,type,imageWidth,imageHeight) {
    this.name = name; 
	this.image = image; 
	this.link = link; 
	this.title = title;
	this.content = content; // this defines if we should be showing an image or a video
	this.type = type; // defines what item we are loading as part of it
	if(typeof imageWidth === 'undefined'){
		imageWidth = 800;
	}
	if(typeof imageHeight === 'undefined'){
		imageHeight = 500;
	}
	this.imageWidth = imageWidth;
	this.imageHeight = imageHeight;
}

// Debug functions to see current state
Project.prototype.Debug = function() {
	 console.log(this.name) ;
}